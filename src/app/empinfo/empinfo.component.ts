import { Component, OnInit } from '@angular/core';
import {MockService} from "./../mock.service";

@Component({
  selector: 'app-empinfo',
  templateUrl: './empinfo.component.html',
  styleUrls: ['./empinfo.component.css']
})
export class EmpinfoComponent implements OnInit {
employees;
  constructor(private ms:MockService)  { }

  ngOnInit() {

    this.ms.getEmployees().subscribe((res)=>this.employees=res);
  }
delEmp(id){
  this.ms.deleteEmployee(id).subscribe();
}
}
