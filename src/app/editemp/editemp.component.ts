import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MockService} from './../mock.service';
import {FormGroup,FormControl,Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-editemp',
  templateUrl: './editemp.component.html',
  styleUrls: ['./editemp.component.css']
})
export class EditempComponent implements OnInit {
  empinfo={};
  empForm;
  constructor(private activated:ActivatedRoute, private ms:MockService, private router:Router) { }

  ngOnInit() {
   let eid= this.activated.snapshot.params["eid"];
   this.ms.getEmployeeById(eid).subscribe((res)=>this.empinfo=res);
   this.empForm= new FormGroup({
     id: new FormControl(),
    name: new FormControl(),
     desg: new FormControl(),
     sal: new FormControl(),
   })
  }

  updateEmp(data){
    let id=data.id;
    if(this.empForm.valid){
    let id=this.empForm.controls.id.value;
    //let data=this.empForm.value;
    this.ms.updateEmployee(id,data).subscribe((res)=>console.log(res));
  }
   this.router.navigate(['/']);
  }

}
