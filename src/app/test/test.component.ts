import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styles: [`
  
  div{
    color:red;
  }

  .text-success{
    color:green;
  }

  .text-danger{
    color:red;
  }

  .text-special{
    font-style:italic;
  }
  `]
})
export class TestComponent implements OnInit {

  public name ="vishwas";
  public greetings="";
  displayName= false;
  public color="red";

  public person={
    "firstName":"john",
    "lastName":"Doe"
  }
  constructor() { }

  ngOnInit() {
  }
  onClick(){
    console.log("welcome to website");
    this.greetings="welcome mandela";
  }
} 
