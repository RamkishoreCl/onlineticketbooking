import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {MockService} from './../mock.service';
import {FormBuilder,Validators} from '@angular/forms';


@Component({
  selector: 'app-addemp',
  templateUrl: './addemp.component.html',
  styleUrls: ['./addemp.component.css']
})
export class AddempComponent implements OnInit {
  newemp={};
  addForm;

  constructor(private router:Router, private ms:MockService, private fb:FormBuilder) { }

  ngOnInit() {
    this.addForm=this.fb.group({
      id:[],
      name:[],
      desg:[],
      sal:[]
    })
  }

  addEmp(){

    if(this.addForm.valid){

    this.ms.addEmployee(this.addForm.value).subscribe();
    this.router.navigate(['/']);
  }
}

}
