import { Component, OnInit } from '@angular/core';
import{FormBuilder,Validators} from '@angular/forms';
import {MockService} from './../mock.service';
import {SharingdataService} from './../sharingdata.service';
import {map,catchError} from 'rxjs/operators';
import {Router  } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  logForm;
  errMsg;

  constructor(private fb:FormBuilder, private ms:MockService, private sds:SharingdataService, private router: Router) { }

  ngOnInit() {

    this.logForm=this.fb.group({

      uname:['',[Validators.required]],
      mail:['',[Validators.required]]
    })
  }
loginPage(){
  if(this.logForm.valid){
   
   console.log(this.logForm.value);
   this.ms.getUserByEmailAndUserName(this.logForm.value.mail,this.logForm.value.uname).subscribe(
     (data)=>{
         if(Object(data).length>0){
          this.errMsg="" ;
          console.log(data);
             this.sds.shareData(data);
             this.router.navigate(['/home'])
         }
         else{
          this.errMsg="Invalid credentials";
         }
        })
 
     
  } else{
   this.errMsg="Enter email and username";
  }
}

}
