import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class MockService {

  constructor(private http:HttpClient) { }
  
  getEmployees(){
    
      return this.http.get("http://localhost:3000/employees");
    }
    getEmployeeById(id){
      return this.http.get("http://localhost:3000/employees/"+id);
    }

    addEmployee(data){
      let headers={"content-Type":"application/json"};
      return this.http.post("http://localhost:3000/employees/",data,{headers:headers});
    }

    updateEmployee(id,data){
      let headers={"content-Type":"application/json"};
      return this.http.put("http://localhost:3000/employees/"+id,data,{headers:headers})
    }

    deleteEmployee(id){
      let headers={"content-Type":"application/json"};
      return this.http.delete("http://localhost:3000/employees/"+id)
    }

    getUserByEmailAndUserName(email,uname){
      console.log("http://localhost:3000/users/?email"+"= "+email+"&username="+uname);
      return this.http.get("http://localhost:3000/users/?email"+"= "+email+"&username="+uname);
    }
  
















    




}
