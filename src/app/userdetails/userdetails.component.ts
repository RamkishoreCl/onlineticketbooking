import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router  } from "@angular/router";

@Component({
  selector: 'app-userdetails',
  templateUrl: './userdetails.component.html',
  styleUrls: ['./userdetails.component.css']
})
export class UserdetailsComponent implements OnInit {
  users;
  


  constructor(private hc: HttpClient, private rout:Router) { }

  ngOnInit() {
    this.hc.get("https://jsonplaceholder.typicode.com/users").subscribe((res)=>this.users=res)
  }
  

   getcoursename(value){
     if(value=="http://localhost:3000/posts"){
      this.rout.navigate(['/posts']);
     }
     else if(value=="albums"){
      this.rout.navigate(['/albums']);
     }
     else if(value=="comments"){
      this.rout.navigate(['/comments']);
     }
     else{
      this.rout.navigate(['/todos']);
     }
    

  }
  
}
