import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SampleComponent } from './sample/sample.component';
import { TdformComponent } from './tdform/tdform.component';
import { FcfgformComponent } from './fcfgform/fcfgform.component';
import { FbformComponent } from './fbform/fbform.component';
import { TestComponent } from './test/test.component';
import { TdfComponent } from './tdf/tdf.component';
import { FomComponent } from './fom/fom.component';
import { TdformsComponent } from './tdforms/tdforms.component';
import { FgfcsComponent } from './fgfcs/fgfcs.component';
import {RouterModule} from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { ServicesComponent } from './services/services.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import {HttpClientModule} from '@angular/common/http';
import { UserdetailsComponent } from './userdetails/userdetails.component';
import { EdituserComponent } from './edituser/edituser.component';
import { EmpinfoComponent } from './empinfo/empinfo.component';
import { EditempComponent } from './editemp/editemp.component';
import { AddempComponent } from './addemp/addemp.component';
import { ParentComponent } from './parent/parent.component';
import { ChildComponent } from './child/child.component';
import { FirstComponent } from './first/first.component';
import { SecondComponent } from './second/second.component';
import { LoginComponent } from './login/login.component';
import { ReversePipe } from './reverse.pipe';

@NgModule({
  declarations: [
    AppComponent,
    SampleComponent,
    TdformComponent,
    FcfgformComponent,
    FbformComponent,
    TestComponent,
    TdfComponent,
    FomComponent,
    TdformsComponent,
    FgfcsComponent,
    HomeComponent,
    ProfileComponent,
    ServicesComponent,
    PagenotfoundComponent,
    UserdetailsComponent,
    EdituserComponent,
    EmpinfoComponent,
    EditempComponent,
    AddempComponent,
    ParentComponent,
    ChildComponent,
    FirstComponent,
    SecondComponent,
    LoginComponent,
    ReversePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot([
     
      {path:"",component:EmpinfoComponent},
      //{path:"",component:LoginComponent},
      {path:"home",component:HomeComponent},
      //{path:"empinfo",component:EmpinfoComponent},
      {path:"editemp/:eid",component:EditempComponent},
      {path:"addemp",component:AddempComponent},
      {path:"services",component:ServicesComponent},
      {path:"profile",component:ProfileComponent},
      {path:"parent",component:ParentComponent},
      {path:"first",component:FirstComponent},
      {path:"second",component:SecondComponent},
      {path:"user",component:UserdetailsComponent},
      {path:"edit/:uid",component:EdituserComponent},
      {path:'**',component:PagenotfoundComponent}

      
     
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
