import { Component, OnInit } from '@angular/core';
import { SharingdataService } from "./../sharingdata.service";

@Component({
  selector: 'app-second',
  templateUrl: './second.component.html',
  styleUrls: ['./second.component.css']
})
export class SecondComponent implements OnInit {

  constructor(private sds:SharingdataService) { }

  ngOnInit() {
    this.sds.fetchProps.subscribe((data)=>console.log(data));
  }

}
