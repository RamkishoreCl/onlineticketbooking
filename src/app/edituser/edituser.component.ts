import { Component, OnInit } from '@angular/core';
import {FormBuilder,Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {HttpClient} from '@angular/common/http';


@Component({
  selector: 'app-edituser',
  templateUrl: './edituser.component.html',
  styleUrls: ['./edituser.component.css']
})
export class EdituserComponent implements OnInit {
editForm;
userinfo={};

  constructor( private fb:FormBuilder, private http:HttpClient, private avtiveR:ActivatedRoute) { }

  ngOnInit() {

    this.editForm = this.fb.group({

      uid:['',Validators.required],
      name:[],
      uname:[],
      mail:[],
      ustate:[],
      ucity:[],
      street:[],
      zip:[]


    })
    

   let id = this.avtiveR.snapshot.params["uid"];
   this.http.get("https://jsonplaceholder.typicode.com/users/"+id).subscribe((res)=>console.log(res))
  }

 

}
