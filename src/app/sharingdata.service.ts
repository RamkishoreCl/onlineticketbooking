import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";


@Injectable({
  providedIn: 'root'
})
export class SharingdataService {
props:any;
public fetchProps:BehaviorSubject<any>=new BehaviorSubject<any>(this.props);

  constructor() { }
  shareData(data){
    this.props=data;
    this.fetchProps.next(this.props)
  }

  retData(){
    return this.props;
  }
}
