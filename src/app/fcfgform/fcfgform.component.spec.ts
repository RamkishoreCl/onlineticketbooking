import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FcfgformComponent } from './fcfgform.component';

describe('FcfgformComponent', () => {
  let component: FcfgformComponent;
  let fixture: ComponentFixture<FcfgformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FcfgformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FcfgformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
