import { Component, OnInit } from '@angular/core';
import {FormGroup,FormControl,Validators} from '@angular/forms';

@Component({
  selector: 'app-fcfgform',
  templateUrl: './fcfgform.component.html',
  styleUrls: ['./fcfgform.component.css']
})
export class FcfgformComponent implements OnInit {

  regForm;

  constructor() { }

  ngOnInit() {

    this.regForm = new FormGroup({

      empId: new FormControl('',Validators.required)
    })
  }

}
