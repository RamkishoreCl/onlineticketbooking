import { Component, OnInit } from '@angular/core';
import {SharingdataService} from './../sharingdata.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})
export class FirstComponent implements OnInit {

  constructor(private sds:SharingdataService, private router:Router) { }

  ngOnInit() {
  }

  sendData(){
    let employee={id:101,name:"bhanu", desg:"cm", sal:6500};
    this.sds.shareData(employee);
    this.router.navigate(['/second'])
  }

}
