import { Component, OnInit } from '@angular/core';
import { SharingdataService } from './../sharingdata.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private sds:SharingdataService) { }

  ngOnInit() {

    this.sds.fetchProps.subscribe((data)=>console.log(data));
  }

}
