import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {
  @Input() parentProp:string;
  @Input() empinfo:object;
  @Output() childProps = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
    console.log(this.parentProp);
    //console.log(this.empinfo);
  }
childMeth(){
  console.log("child method");
  this.childProps.emit({id:1,name:"michel", age:26});
  console.log(this.childProps);
}
}
