import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit {

  employees=[
    {"id":101, "name":"michel", "desg":"pm", "sal":9000},
    {"id":102, "name":"john", "desg":"cm", "sal":8000},
    {"id":103,"name":"vinz","desg":"wk","sal":7000},
    {"id":104,"name":"vinzyy","desg":"wlk","sal":6000}
     ];

  constructor() { }

  ngOnInit() {
  }
 retChildData(eve){
   console.log(eve);
 }
}
