import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FgfcsComponent } from './fgfcs.component';

describe('FgfcsComponent', () => {
  let component: FgfcsComponent;
  let fixture: ComponentFixture<FgfcsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FgfcsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FgfcsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
