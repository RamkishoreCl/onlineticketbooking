import { Component, OnInit } from '@angular/core';
import {FormGroup,FormControl, Validators} from '@angular/forms';
import { variable } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-fgfcs',
  templateUrl: './fgfcs.component.html',
  styleUrls: ['./fgfcs.component.css']
})
export class FgfcsComponent implements OnInit {
 regForm;
  constructor() { }

  ngOnInit() {

    this.regForm= new  FormGroup({
    empId: new FormControl('',[Validators.required,Validators.pattern('^[0-9]+$'),Validators.minLength(3),Validators.maxLength(15)]),
    empName: new FormControl('',[Validators.required,Validators.pattern('^[a-zA-Z]+$')])
    });
  }

}
