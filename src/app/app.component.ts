import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  //title = 'samplepro';
  firstname:string ="michel johnson";
  isApproved:boolean=true;
  names:string[]=["mike","hussey","David"];
  first:string="michel";

  details(){
    console.log("details");
  }
}
