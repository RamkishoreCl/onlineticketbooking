import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fom',
  templateUrl: './fom.component.html',
  styleUrls: ['./fom.component.css']
})
export class FomComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

   nameController($scope){
    $scope.firstName = 'Claude';
    $scope.lastName = 'Debussy';
}

}
